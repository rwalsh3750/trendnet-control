/*
trendnet_control.h

Created: Oct 16, 2015
Author: Ryan W Walsh

- This header's objective is to facilitate communications to and from an IP camera over the Internet
- The camera this header was specifically designed for was the TrendNet TV-IP672WI (other cameras may differ)
- Note that not all camera functions are included
	- (such functions include configuring the camera's WIFI, changing the password...., but other more useful functions could be included)
- The get and Post methods are nearly identical
	- The IP672WI almost always returns some char after doing a post (the set functions if such char is available is returned for debugging purposes)
		- Please see the attached pdfs for more information
- Things to consider
	- Provide functionality for communication in a LAN environment (See pdfs)
	- Add remaining functions (if necessary)
	- Remove the setSpeed function (this method is misleading as TV-IP672WI has no such functionality)
- An example program that utilizes this header/class is the commented out main method found at the bottom of trendnet_control.c
*/

#ifdef __cplusplus
extern "C" {
	#endif

	#ifndef TRENDNET_CONTROL_H
	#define TRENDNET_CONTROL_H

	#include "curl.h"

	CURL *curl;
	CURLcode res; //Error info dump

	typedef struct {
		char *ipAddress, *username, *password;
	} ipCamera;

	char* trendnet_control_setPTAbsolute(ipCamera *self, int pan, int tilt);
	char* trendnet_control_setPTRelative(ipCamera *self, int pan, int tilt);
	char* trendnet_control_setSpeed(ipCamera *self, int direction, int speed);
	char* trendnet_control_getPT(ipCamera *self);
	char* trendnet_control_getImage(ipCamera *self);
	char* trendnet_control_getPTBoundries(ipCamera *self);
	char* trendnet_control_setVideoConfig(ipCamera *self, int profileid, char resolution[], int bitrate, char codec[], int framerate);
	char* trendnet_control_getVideoConfig(ipCamera *self, int profileid);

	char* trendnet_control_post(char *url, char *postData, char *username, char *password);
	char* trendnet_control_get(char *url, char *username, char *password);
	size_t trendnet_control_write_callback_func(void *buffer, size_t size, size_t nmemb, void *userp);

	void trendnet_control_Start(void);
	void trendnet_control_Destroy(void);
	ipCamera new_ipCamera(char *ipAddress, char *username, char *password);

	#endif

	#ifdef __cplusplus
}
#endif
