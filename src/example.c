/* The following is an example program for interfacing with a trendnet camera
 * There are a few quarks with its interface and some of these have been commented on. */

#include "trendnet_control.c"

int main(void){
	char address[]="YOURIPADDRESSHERE", username[]="admin", password[]="password"; //Define params for my new camera
	
	trendnet_control_Start(); //Must initialize global curl process
	ipCamera myCamera = {address, username, password}; //Setup new camera

	printf("Camera Boundaries:\n%s", trendnet_control_getPTBoundries(&myCamera));  //Get camera movement boundries
	
	// Move camera absolute
	printf("Move Camera Absolute (Input: p=50, t=50):\n");
	trendnet_control_setPTAbsolute(&myCamera, 50, 50);
	printf("Current Camera PT:\n%s", trendnet_control_getPT(&myCamera)); //Get camera output
	
	//Move camera relative
	printf("Move Camera Relative (Input: p=30, t=-30):\n");
	trendnet_control_setPTRelative(&myCamera, 30, -30);
	printf("Current Camera PT:\n%s", trendnet_control_getPT(&myCamera));  //Get camera output
	
	printf("Printing Set Relative (Input: p=-30, t=30):\n%s", trendnet_control_setPTRelative(&myCamera, -30, 30)); //Notice how this never return actual PT, only what you just told the camera

	trendnet_control_Destroy(); //Always do a global cleanup or won't end the curl process
	return EXIT_SUCCESS;
}
