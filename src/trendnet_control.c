#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <curl.h>
#include "trendnet_control.h"

/*
Create new camera
*/
ipCamera new_ipCamera(char *ipAddress, char *username, char *password){
    ipCamera this;

    //Copy over vars
    this.ipAddress = strdup(ipAddress);
    this.username = strdup(username);
    this.password = strdup(password);

    return this;
}

/*
Inits the trendnet_control class
Always invoke this function at least once near the start of your program

Notice that this function uses a method that is not thread safe
 */
void trendnet_control_Start(void){
	curl_global_init(CURL_GLOBAL_ALL);// In windows, this will be init the winsock stuff
}

/*
Destroys remaining leftovers of the trendnet_control class

Always invoke this function to properly end curl threads
 */
void trendnet_control_Destroy(void){
	curl_global_cleanup();
}

/*
Sets camera position

Notice errors may occur if you request a change that puts the camera positions out of range
	Equivalent terminal command: "curl -u USERNAME:PASSWORD http://IPADDRESS/config/ptz_info.cgi" (For TV-IP672WI) to get information about bounds
	Equivalent terminal command: "curl -u USERNAME:PASSWORD http://IPADDRESS/config/ptz_pos.cgi" (For TV-IP672WI) to get information about current position
*/
char* trendnet_control_setPTAbsolute(ipCamera *self, int pan, int tilt){
	char command[] = "/config/ptz_move.cgi";
	char tempString[8];
	int lenVars = sprintf(tempString, "%d%d", pan, tilt);
	char postFields[lenVars+5], url[strlen(self->ipAddress)+strlen(command)+1];

	sprintf(url, "%s%s", self->ipAddress, command);
	sprintf(postFields, "p=%d&t=%d", pan, tilt);
	return trendnet_control_post(url, postFields, self->username, self->password);
}

/*
Sets camera position relative to current position

Notice errors will occur if you request a change that puts the camera positions out of range
	Equivalent terminal command: "curl -u USERNAME:PASSWORD http://IPADDRESS/config/ptz_info.cgi" (For TV-IP672WI) to get information about bounds
	Equivalent terminal command: "curl -u USERNAME:PASSWORD http://IPADDRESS/config/ptz_pos.cgi" (For TV-IP672WI) to get information about current position
*/
char* trendnet_control_setPTRelative(ipCamera *self, int pan, int tilt){
	char command[] = "/config/ptz_move_rel.cgi";
	char tempString[8];
	int lenVars = sprintf(tempString, "%d%d", pan, tilt);
	char postFields[lenVars+5], url[strlen(self->ipAddress)+strlen(command)+1];

	sprintf(url, "%s%s", self->ipAddress, command);
	sprintf(postFields, "p=%d&t=%d", pan, tilt);
	return trendnet_control_post(url, postFields, self->username, self->password);
}

/*
Sets Camera Speed values 1-10

Direction: 0-13
	0: wide (zoom out) with stop.
	1: up with stop
	2: tele (zoom in) with stop
	3: left with stop
	4: home (only for custom command)
	5: right with stop
	6: focus far with stop
	7: down with stop
	8: focus near with stop
	9:
	10: custom command 1
	11: custom command 2
	12: custom command 3
	13: custom command 4

Speed: 1-10
	speed control for up, down, left, right. (1-10)
	(includes: Dyna, Lilin, Lilin2, PelcoD, PelcoP)
*/
char* trendnet_control_setSpeed(ipCamera *self, int direction, int speed){
	ipCamera *this = self;
	char command[] = "/config/rs485_do.cgi";
	char tempString[8];
	int lenVars = sprintf(tempString, "%d%d", direction, speed);
	char postFields[lenVars+17], url[strlen(this->ipAddress)+strlen(command)+1];

	sprintf(url, "%s%s", this->ipAddress, command);
	sprintf(postFields, "direction=%d&speed=%d", direction, speed);
	return trendnet_control_post(url, postFields, this->username, this->password);
}

/*
Configures camera video stream options for chosen profileid(log on to camera in browser for more intuitive sense of what this is doing)
(This function has net been tested)
	Equivalent terminal command: "curl -u USERNAME:PASSWORD http://IPADDRESS/config/group_list.cgi" (For TV-IP672WI) to get information about the options available for each field

1. No support goplength field.
2. The quality field is only used
in MJPEG codec type.
3. The bitrate field is only used
in MPEG4 codec type.
4. The quality field has the
following define:
setting getting(response)
01~60 (0) 55
61~70 (1) 65
71~80 (2) 75
81~90 (3) 85
91~100 (4) 95
5. The codec type of profile3 is
always MJPEG.
6. The codec type of profile4 is
always MPEG4.
7. After setting change
*/
char* trendnet_control_setVideoConfig(ipCamera *self, int profileid, char resolution[], int bitrate, char codec[], int framerate){
	ipCamera *this = self;
	char command[] = "/config/video.cgi";
	char tempString[32];
	int lenVars = sprintf(tempString, "%d%s%d%s%d", profileid, resolution, bitrate, codec, framerate);
	char postFields[lenVars+49], url[strlen(this->ipAddress)+strlen(command)+1];

	sprintf(url, "%s%s", this->ipAddress, command);
	sprintf(postFields, "profileid=%d&resolution=%s&bitrate=%d&codec=%s&framerate=%d", profileid, resolution, bitrate, codec, framerate);
	return trendnet_control_post(url, postFields, this->username, this->password);
}

/*
Gets current camera stream config selections for chosen profileid (log on to camera in browser for more intuitive sense of what this is doing)
*/
char* trendnet_control_getVideoConfig(ipCamera *self, int profileid){
	ipCamera *this = self;
	char command[] = "/config/video.cgi";
	char tempString[4];
	int lenVars = sprintf(tempString, "%d", profileid);
	char postFields[lenVars+10], url[strlen(this->ipAddress)+strlen(command)+1];

	sprintf(url, "%s%s", this->ipAddress, command);
	sprintf(postFields, "profileid=%d", profileid);
	return trendnet_control_post(url, postFields, this->username, this->password);
}

/*
Gets current camera position
Return Format:
	p=int
	t=int
*/
char* trendnet_control_getPT(ipCamera *self){
	ipCamera *this = self;
	char command[] = "/config/ptz_pos.cgi";
	char url[strlen(this->ipAddress)+strlen(command)+1];
	sprintf(url, "%s%s", this->ipAddress, command);

	return trendnet_control_get(url, this->username, this->password);
}

/*
Gets current camera image
Not sure if this works 100% (It returns a JPEG in char string form, so you might not be able to convert)
 */
char* trendnet_control_getImage(ipCamera *self){
	ipCamera *this = self;
	char command[] = "/image/jpeg.cgi";
	char url[strlen(this->ipAddress)+strlen(command)+1];
	sprintf(url, "%s%s", this->ipAddress, command);

	return trendnet_control_get(url, this->username, this->password);
}

/*
Gets camera position boundaries
Return Format:
	pmax=int
	pmin=int
	tmax=int
	tmin=int
	customizedhome=bool
 */
char* trendnet_control_getPTBoundries(ipCamera *self){
	ipCamera *this = self;
	char command[] = "/config/ptz_info.cgi";
	char url[strlen(this->ipAddress)+strlen(command)+1];
	sprintf(url, "%s%s", this->ipAddress, command);

	return trendnet_control_get(url, this->username, this->password);
}

/*
Sends data to camera.

It might be a good idea to return a bool if success or not
*/
char* trendnet_control_post(char *url, char *postData, char *username, char *password){
	curl = curl_easy_init();//Get a curl handle
	char *response;

	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); //Uncomment for debugging curl
	//curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); //Might be needed, but not for TVIP6WI
	curl_easy_setopt(curl, CURLOPT_USERNAME, username); //Set username
	curl_easy_setopt(curl, CURLOPT_PASSWORD, password); //Set password
	//curl_easy_setopt(curl, CURLOPT_PORT, port); //<-Not needed for some cameras, and if not, breaks program when uncommented (not needed for TV-IP672WI)
	curl_easy_setopt(curl, CURLOPT_URL, url); //Specify url
	curl_easy_setopt(curl, CURLOPT_POST, 1); //Set to POST
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, trendnet_control_write_callback_func); //Prevent function from printing to terminal
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response); //Passing the pointer to the response as the callback parameter
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData); //specify post data
	res = curl_easy_perform(curl); //Perform action
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res)); //print error if occurs
	curl_easy_cleanup(curl); // always cleanup
	return response;
}

/*
Gets data to camera.
Returns camera response string.

It might be a good idea to return a bool if success or not

For help, see: http://www.dimuthu.org/blog/2009/01/28/making-web-requests-using-curl-from-c-and-php/
*/
char* trendnet_control_get(char *url, char *username, char *password){
	curl = curl_easy_init();//Get a curl handle
	char *response;

	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); //Uncomment for debugging curl
	curl_easy_setopt(curl, CURLOPT_USERNAME, username); //Set username
	curl_easy_setopt(curl, CURLOPT_PASSWORD, password); //Set password
	curl_easy_setopt(curl, CURLOPT_URL, url); //Specify url
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1); //Set to GET
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1); //Follow locations specified by the response header
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, trendnet_control_write_callback_func); //Setting a callback function to return the data
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response); //Passing the pointer to the response as the callback parameter
	res = curl_easy_perform(curl); //Perform action
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res)); //print error if occurs
	curl_easy_cleanup(curl); // always cleanup
	return response;
}

/*
Invokes function (usrp) as data is received
*/
size_t trendnet_control_write_callback_func(void *buffer, size_t size, size_t nmemb, void *userp){
    char **response_ptr = (char**)userp;
    *response_ptr = (strndup(buffer, (size_t)(size *nmemb))); //Assume response is string
    return ((size_t)(size *nmemb));
}
