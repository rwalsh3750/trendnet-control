# TRENDnet API interface example using C/C++

### Introduction
This project was developed for a larger computer vision project but was discontinued due to the TV-IP672WI camera being found insufficient for the needs of that larger project. Regardless, this project is provided to help anyone who wants to develop for similar cameras.

### Things to Note
An example program is provided for basic interface with trendnet cameras. However, not all functions are included, but these should be easily implementable using the provided pdf documentation.

### Basic Setup Guide
[Setup the camera](http://www.trendnet.com/support/view_faq_question.asp?ToDo=view&catID=552&questID=2575#ratefaq) and get familiar with its internet interface. Edit the example.c file to include the correct IPADDRESS, USERNAME, and PASSWORD for the camera in question using information from that interface. 

Install curl
```
#!terminal
sudo apt-get install curl
```
Compile and run the example from the src folder
```
#!terminal
gcc -o trendnetExample example.c -lcurl -L/usr/lib -I/usr/include/curl
./trendnetExample 
```